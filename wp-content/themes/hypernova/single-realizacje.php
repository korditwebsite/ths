<?php get_header(); ?>
<main class="single-realizacje">
	<section class="details-realizacje">
		<div class="header-top" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
			<div class="position-container">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</section>
	<section class="details-more">
		<div class="container">	
			<div class="row">
				<div class="col-xl-12">
					<h4 class="section-title">szczegóły projektu</h4>
				</div>
				<div class="col-xl-6">
					<h3><?php the_field( 'minitytul' ); ?></h3>
					<address><?php the_field( 'miasto' ); ?></address>
					<div class="more-info-text">
						<?php the_field( 'tresc' ); ?>
					</div>
				</div>
				<div class="col-xl-6">
					<div class="thumbnail">
						<?php echo wp_get_attachment_image( get_field('obrazek'), "full", "", array( "class" => "d-block w-100") ); ?>
					</div>
				</div>		
			</div>
		</div>
	</section>
	<section class="details-gallery">
		<div class="container">
			<div class="col-xl-12">
				<h4 class="section-title">galeria</h4>
			</div>
			<div id="gallery-carousel" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					<?php if ( have_rows( 'galerie' ) ) : $x = 0; while ( have_rows( 'galerie' ) ) : the_row(); ?>
						<?php if ($x == 0) {
							$active = "active";
						} else {
							$active = " ";
						} ?>
						<div class="carousel-item <?php echo $active ?>">
							<div class="grid-content">
								<?php 
								$images = get_sub_field('galeria');
								if( $images ): ?>
									<?php foreach( $images as $image ): ?>
										<a class="single-portfolio-item foobox" rel="gallery-<?php echo $x; ?>" href="<?php echo esc_url($image['url']); ?>">
											<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
										</a>
									<?php endforeach; ?>
								<?php endif; ?>
							</div>
						</div>
						<?php $x = $x +1; endwhile;  endif; ?>
					</div>
					<div class="container-arrow">
						<a class="carousel-control-prev" href="#gallery-carousel" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="carousel-control-next" href="#gallery-carousel" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>

			</div>
		</section>
	</main>
	<?php get_footer(); ?> 