<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="keywords" content="<?php the_field("slowa_kluczowe", "options"); ?>" />
	<?php wp_head(); ?>
	<?php if (is_page_template( 'single.php' )): ?>
		<title><?php wp_title(); ?></title>
	<?php endif ?>
	
</head>
<body <?php body_class( 'class-name' ); ?> data-spy="scroll" data-target="#list-example" data-offset="0">
	<?php
	if ( have_rows( 'header-settings', 'option' ) ) : 
		while ( have_rows( 'header-settings', 'option' ) ) : the_row(); 
			$wyborheadera = get_sub_field( 'header_sekcja' ); 
		endwhile;
	endif;
	?>
	<header id="header-top">
		<div class="my-container">
			<div class="logo-item">
				<a href="/">
					<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
					$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
					if ( has_custom_logo() ) {
						echo '<img alt="logotyp" class="img-fluid logotype" src="'. esc_url( $logo[0] ) .'">';
					} else {
						echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
					} ?>
				</a>
			</div>
			<div class="hamburger-mobile">
				<div class="hamburger" id="hamburger-11">
					<span class="line"></span>
					<span class="line"></span>
					<span class="line"></span>
				</div>
			</div>
			<div class="navigation-item" id="list-example">
				<?php
				wp_nav_menu([
					'menu'            => 'top',
					'theme_location'  => 'top',
					'container_class' => '',
					'menu_id'         => false,
					'depth'           => 2,
					'fallback_cb'     => 'bs4navwalker::fallback',
					'walker'          => new bs4navwalker()
				]);
				?>
			</div>
		</div>
	</header>
	