<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-xl-4 col-md-6">
				<div class="row">
					<?php if ( have_rows( 'dane_kontaktowe', 'options' ) ) : while ( have_rows( 'dane_kontaktowe', 'options' ) ) : the_row(); ?>
						<div class="col-xl-6">
							<div class="single-item-footer">
								<h5><?php the_sub_field( 'tytul' ); ?></h5>

								<div class="item-1"><?php the_sub_field( 'adres_1' ); ?></div>
								<div class="item-2"><?php the_sub_field( 'adres_2' ); ?></div>

							</div>
						</div>
					<?php endwhile; endif; ?>
				</div>
			</div>
			<div class="col-xl-4 offset-xl-1 col-md-6">
				<div class="row">
					<?php if ( have_rows( 'informacje', 'options' ) ) : while ( have_rows( 'informacje', 'options' ) ) : the_row();
						$fb = get_sub_field( 'link_facebook' );
						?>
						<div class="col-xl-12">
							<h5>ths system sp. z o.o.</h5>
						</div>
						<div class="col-xl-6">
							<div class="item-1"><?php the_sub_field( 'adres_1' ); ?></div>
							<div class="item-2"><?php the_sub_field( 'adres_2' ); ?></div>
						</div>
						<div class="col-xl-6">
							<div class="item-3">NIP: <?php the_sub_field( 'nip' ); ?></div>
							<div class="item-4">REGON: <?php the_sub_field( 'REGON' ); ?></div>
						</div>
					<?php endwhile; endif; ?>
				</div>
			</div>
			<div class="col-xl-3 col-md-12">
				<div class="fb">
					Bądź na bieżąco, znajdziesz nas na:
					<a target="_blank" href="<?php echo $fb; ?>">
						<img src="/wp-content/uploads/2020/03/Warstwa-481.png">
					</a>
				</div>
			</div>
			<div class="col-xl-12">
				<div class="copywrite">
					<div class="row">
						<div class="col-xl-6 col-md-6">
							<span>© THS System 2020 | Terms and conditions</span>
						</div>
						<div class="col-xl-6 col-md-6">
							<span>design by: Emtigo Design</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
