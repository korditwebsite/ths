<?php
//Inicjacja plików motywu
function theme_enqueue_scripts() {
	wp_enqueue_style( 'normalize', get_template_directory_uri() . '/assets/css/normalize.css' );
	wp_enqueue_style( 'Bootstrap_css', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/assets/css/animate.css' );
	wp_enqueue_style( 'Style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'owl', get_template_directory_uri() . '/assets/css/owl.css' );
	wp_enqueue_style( 'gallery-css', get_template_directory_uri() . '/assets/includes/general/ecogallery/gallery.css' );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );

function my_custom_script_load(){
	wp_enqueue_script( 'jQuery', get_template_directory_uri() . '/assets/js/jquery.js', array(), '3.3.1', true );
	wp_enqueue_script( 'bundle',  get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js', array(), '3.3.1', true );
	wp_enqueue_script( 'Bootstrap',  get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), '3.3.1', true );
	wp_enqueue_script( 'apear', get_template_directory_uri() . '/assets/js/apear.js', array());
	wp_enqueue_script( 'Main', get_template_directory_uri() . '/assets/js/initial.js', array(), '1.0.0', true );
	wp_enqueue_script( 'owl', get_template_directory_uri() . '/assets/js/owl.js', array(), '1.0.0', true );
	wp_enqueue_script( 'my-js', get_template_directory_uri() . '/assets/js/myjs.js', array(), '1.0.0', true );
	get_template_part( '/assets/includes/setup'); 
}
add_action( 'wp_enqueue_scripts', 'my_custom_script_load' );



//Dodawanie bibliotek z pozycji panelu admina


add_filter( 'wpcf7_autop_or_not', '__return_false' );


//Dodawanie wsparcia dla motywu
get_template_part( '/assets/includes/support'); 

// //Dodawanie wsparcia dla CPT
get_template_part( '/assets/includes/cpt'); 

// //Dodawanie widgetów
get_template_part( '/assets/includes/widget'); 

// //Dodawanie rozmiarów miniaturek
get_template_part( '/assets/includes/size-thumbnail'); 

//Tworzenie automatycznej struktury nawigacji
get_template_part( '/assets/includes/walker-nav-menu'); 

// Register WordPress nav menu
register_nav_menu('top', 'Top menu');
register_nav_menu('footer', 'Footer menu');
register_nav_menu('menu', 'Karta menu');
function add_svg_to_upload_mimes( $types ) {
	$types[ 'svg' ] = 'image/svg+xml';
	return $types;
}

add_filter( 'upload_mimes', 'add_svg_to_upload_mimes' );



