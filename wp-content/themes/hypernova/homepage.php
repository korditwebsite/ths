<?php
/**
* Template Name: Strona główna
*/
?>
<?php get_header(); ?>
<main>
	<?php if ( have_rows( 'slider' ) ) : $x = 0; $y = 0; $counter = 0;
		while ( have_rows( 'slider' ) ) : the_row();
			$counter = $counter +1; 
		endwhile;
		?>

		<section class="slider" id="slider">
			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<?php while ( have_rows( 'slider' ) ) : the_row();
						if ($x == 0) {
							$active = "active";
						} else {
							$active = " ";
						}
						?>
						<li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $x; ?>" class="<?php echo $active; ?>">

						</li>
						<?php $x = $x +1; endwhile; ?>
					</ol>
					<div class="carousel-inner">
						<?php while ( have_rows( 'slider' ) ) : the_row();
							if ($y == 0) {
								$active2 = "active";
							} else {
								$active2 = " ";
							}
							$set = get_sub_field( 'wybierz_ulozenie' );
							?>
							<div class="carousel-item <?php echo $active2;  ?>">
								<?php if ($set == 1): ?>
									<div class="image-slider">
										<?php echo wp_get_attachment_image( get_sub_field('grafika'), "full", "", array( "class" => "d-block w-100") ); ?>
										<div class="container-text">
											<h4 class="slider-text"><?php the_sub_field( 'tresc' ); ?></h4>
										</div>
									</div>
								<?php endif; ?>
								<?php if ($set == 2): ?>
									<div class="video-slider">
										<video src="<?php the_sub_field( 'video' ); ?>" muted autoplay loop></video>
										<div class="container-text">
											<h4 class="slider-text wow zoomIn"><?php the_sub_field( 'tresc' ); ?></h4>
										</div>
									</div>
								<?php endif; ?>
							</div>
							<?php $y = $y +1; endwhile; ?>
						</div>
					</div>
				</section>
				<section class="about-us" id="about-us">
					<?php if ( have_rows( 'o_nas' ) ) : while ( have_rows( 'o_nas' ) ) : the_row(); ?>
						<div class="container">
							<div class="row">
								<div class="col-xl-12">
									<h4 class="section-title">O nas</h4>
								</div>
								<?php  if ( have_rows( 'zalety' ) ) : $time = 0; while ( have_rows( 'zalety' ) ) : the_row(); ?>
									<div class="col-xl-4">
										<div class="single-item wow zoomIn" data-wow-delay="<?php echo $time; ?>ms">
											<h3><?php the_sub_field( 'naglowek' ); ?></h3>
											<div class="description">
												<?php the_sub_field( 'opis' ); ?>
											</div>
										</div>
									</div>
									<?php $time = $time + 250; endwhile; endif; ?>
								</div>
							</div>
						<?php endwhile; endif; ?>
					</section>
					<section class="counters" id="counter">
						<div class="container">
							<div class="row">
								<?php if ( have_rows( 'liczniki' ) ) :$time = 0; while ( have_rows( 'liczniki' ) ) : the_row(); ?>
									<div class="col-xl-2 col-md-4 single-counter wow zoomIn" data-wow-delay="<?php echo $time; ?>ms">
										<div class="circle" >
											<div class="counter-value" data-count="<?php the_sub_field( 'liczba' ); ?>">
												0
											</div>
										</div>
										<div class="textdesc">
											<?php the_sub_field( 'podpis' ); ?>
										</div>
									</div>
									<?php $time = $time + 125; endwhile; endif; ?>
								</div>
							</div>
						</section>
						<section class="portfolio" id="portfolio">
							<div class="container">
								<div class="row">
									<div class="col-xl-12">
										<h4 class="section-title">Realizacje</h4>
									</div>
									<div class="all-portfolio wow zoomIn">
										<ul class="nav" id="myTab" role="tablist">
											<li class="nav-item">
												<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Inwestycje prywatne</a>
											</li>
											<li class="nav-item">
												<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">inwestycje komercyjne</a>
											</li>

										</ul>
										<div class="tab-content" id="myTabContent">
											<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
												<div class="grid-content">
													<?php
													$namefield = 'galleries';
													$cat = get_queried_object()->term_id;
													$args = array(
														'post_type'   => "realizacje",
														'post_status' => 'publish',
														'order' => 'ASC',
														'posts_per_page'=>'6',
														'tax_query' => [
															[

																'taxonomy' => 'rodzaj',
																'field' => 'term_id',
																'terms' => "4",
															]
														],
													);
													?>
													<?php $testimonials = new WP_Query( $args ); 
													if( $testimonials->have_posts() ) :
														?>
														<?php
														while( $testimonials->have_posts() ) :
															$testimonials->the_post();?>

															<div class="single-portfolio-item" style="background-image: url(<?php the_post_thumbnail_url( 'post-thumbnail' ); ?>);">
																<div class="inner-single-portfolio">
																	<h4><?php the_title(); ?></h4>
																	<h5><?php the_field('miasto'); ?></h5>
																	<a href="<?php echo get_permalink(); ?>">zobacz realizacje</a>
																</div>
																<a class="mobile-more" href="<?php echo get_permalink(); ?>"><img src="https://w32.kordit.pl/wp-content/uploads/2020/03/link.png"></a>
															</div>
															<?php
														endwhile;
														wp_reset_postdata();
														?>
														<?php
													else :
														esc_html_e( 'Kategoria w trakcie uzupełniania, zapraszamy wkrótce!', 'text-domain' );
													endif;
													?>
												</div>
											</div>
											<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
												<div class="grid-content">
													<?php
													$namefield = 'galleries';
													$cat = get_queried_object()->term_id;
													$args = array(
														'post_type'   => "realizacje",
														'post_status' => 'publish',
														'order' => 'ASC',
														'posts_per_page'=>'6',
														'tax_query' => [
															[

																'taxonomy' => 'rodzaj',
																'field' => 'term_id',
																'terms' => "3",
															]
														],
													);
													?>
													<?php $testimonials = new WP_Query( $args ); 
													if( $testimonials->have_posts() ) :
														?>
														<?php
														while( $testimonials->have_posts() ) :
															$testimonials->the_post();?>

															<div class="single-portfolio-item" style="background-image: url(<?php the_post_thumbnail_url( 'post-thumbnail' ); ?>);">
																<div class="inner-single-portfolio">
																	<h4><?php the_title(); ?></h4>
																	<h5><?php the_field('miasto'); ?></h5>
																	<a href="<?php echo get_permalink(); ?>">zobacz realizacje</a>
																</div>
															</div>
															<?php
														endwhile;
														wp_reset_postdata();
														?>
														<?php
													else :
														esc_html_e( 'Kategoria w trakcie uzupełniania, zapraszamy wkrótce!', 'text-domain' );
													endif;
													?>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</section>
						<?php if ( have_rows( 'paralax' ) ) : while ( have_rows( 'paralax' ) ) : the_row(); ?>
							<section class="paralax" style="background-image:url(<?php echo wp_get_attachment_image_url( get_sub_field('tlo'), "full" ); ?>);">
								<div class="logotype wow zoomIn">
									<?php echo wp_get_attachment_image( get_sub_field('logotyp'), "full", "", array( "class" => "d-block") ); ?>
								</div>
							</section>
						<?php endwhile; endif; ?>
						<section class="steps">
							<div class="container">
								<div class="row">
									<div class="col-xl-12">
										<h4 class="section-title">Zobacz jak pracujemy</h4>
									</div>
								</div>
								<div class="rows">
									<?php if ( have_rows( 'kroki' ) ) : $step = 1; while ( have_rows( 'kroki' ) ) : the_row(); ?>
										<div class="single-step wow zoomIn">
											<div class="col-xl-6">
												<div class="inner-row">
													<div class="numbers">
														<div class="circle">
															<div class="label">
																<span>0<?php echo $step ?></span> Krok
															</div>
														</div>
													</div>
													<div class="thumbnail">
														<?php echo wp_get_attachment_image( get_sub_field('grafika'), "full", "", array( "class" => "d-block img-fluid") ); ?>
													</div>
												</div>
											</div>
											<div class="col-xl-6">
												<div class="inner-text-row">
													<h3><?php the_sub_field( 'tytul' ); ?></h3>
													<p><?php the_sub_field( 'podpis' ); ?></p>
												</div>
											</div>
										</div>
										<?php $step = $step + 1; endwhile; endif; ?>
									</div>
								</div>
							</section>
							<?php if ( have_rows( 'paralax_2' ) ) : while ( have_rows( 'paralax_2' ) ) : the_row(); ?>
								<section class="paralax" style="background-image:url(<?php echo wp_get_attachment_image_url( get_sub_field('tlo'), "full" ); ?>);">
									<div class="logotype wow zoomInLeft">
										<?php echo wp_get_attachment_image( get_sub_field('logotyp'), "full", "", array( "class" => "d-block") ); ?>
									</div>
								</section>
							<?php endwhile; endif; ?>
						<?php endif;  ?>
						<?php if ( have_rows( 'zespol' ) ) : while ( have_rows( 'zespol' ) ) : the_row(); ?>
							<section class="team">
								<div class="container">
									<div class="row">
										<div class="col-xl-4">
											<div class="sidebar-image wow zoomInLeft">
												<?php echo wp_get_attachment_image( get_sub_field('grafika'), "full", "", array( "class" => "img-fluid") ); ?>
											</div>
										</div>
										<div class="col-xl-8">
											<div class="all-teammates wow zoomInRight">
												<div class="row">
													<div class="col-xl-12">
														<h4 class="section-title">Nasz zespół</h4>
													</div>
													<?php if ( have_rows( 'czlonkowie_zespolu' ) ) : while ( have_rows( 'czlonkowie_zespolu' ) ) : the_row(); ?>
														<div class="col-xl-6 col-md-6 single-team-item">
															<div class="titles-name">
																<div class="circle">
																	<div class="label-name">
																		<?php the_sub_field( 'imie_i_nazwisko' ); ?>
																	</div>
																</div>
															</div>
															<div class="more-info">
																<div class="position">
																	<?php the_sub_field( 'podtytul' ); ?>
																</div>
																<div class="phone">
																	<a href="tel:<?php the_sub_field( 'numer_telefonu' ); ?>"><?php the_sub_field( 'numer_telefonu' ); ?></a>
																</div>
																<div class="mail">
																	<a href="mailto:<?php the_sub_field( 'mail' ); ?>"><?php the_sub_field( 'mail' ); ?></a>
																</div>
															</div>
														</div>
													<?php endwhile; endif; ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
							<section class="opinion">
								<div class="container">
									<div class="col-xl-12">
										<h4 class="section-title">Opinie klientów</h4>
									</div>
									<div class="owl-carousel owl-carousel-opinion owl-theme wow zoomInLeft">
										<?php if ( have_rows( 'opinie' ) ) : while ( have_rows( 'opinie' ) ) : the_row(); ?>
											<div>
												<div class="single-item-opinion">
													<div class="thumbnail">
														<?php echo wp_get_attachment_image( get_sub_field('grafika'), "full", "", array( "class" => "img-fluid") ); ?>
													</div>
													<div class="quote">
														<?php the_sub_field( 'opinia' ); ?>
													</div>
													<h5>
														<?php the_sub_field( 'imie_i_nazwisko' ); ?>
														<span><?php the_sub_field( 'stanowisko' ); ?></span>
													</h5>
												</div>
											</div>
										<?php endwhile; endif; ?>
									</div>
								</div>
							</section>
							<section class="carousel">
								<div class="my-container wow zoomInLeft">
									<div class="col-xl-12">
										<h4 class="section-title">firmy i branżyści z którymi pracowaliśmy podczas inwestycji</h4>
									</div>
									<div class="owl-carousel owl-carousel-logos owl-theme">
										<?php if ( have_rows( 'karuzela_logotypow' ) ) : while ( have_rows( 'karuzela_logotypow' ) ) : the_row(); ?>
											<div>
												<div class="single-carousel-item">
													<a target="_blank" href="<?php the_sub_field( 'link' ); ?>">
														<?php echo wp_get_attachment_image( get_sub_field('grafika'), "full", "", array( "class" => "") ); ?>
														<label><?php the_sub_field( 'tekst' ); ?></label>
													</a>
												</div>
											</div>
										<?php endwhile; endif; ?>
									</div>
								</div>
							</section>
							<section class="carousel">
								<div class="my-container wow zoomInLeft">
									<div class="col-xl-12">
										<h4 class="section-title">producenci</h4>
									</div>
									<div class="owl-carousel owl-carousel-logos owl-theme">
										<?php if ( have_rows( 'producenci' ) ) : while ( have_rows( 'producenci' ) ) : the_row(); ?>
											<div>
												<div class="single-carousel-item">
													<a target="_blank" href="<?php the_sub_field( 'link' ); ?>">
														<?php echo wp_get_attachment_image( get_sub_field('grafika'), "full", "", array( "class" => "") ); ?>
														<label><?php the_sub_field( 'tekst' ); ?></label>
													</a>
												</div>
											</div>
										<?php endwhile; endif; ?>
									</div>
								</div>
							</section>
						<?php endwhile; endif; ?>
					</main>
					<?php get_footer(); ?> 
