$(window).scroll(function() {    
	var scroll = $(window).scrollTop();

     //>=, not <=
     if (scroll >= 500) {
        //clearHeader, not clearheader - caps H
        $("header").addClass("affix");
    }
}); //missing );

// Select all links with hashes

var a = 0;
$(window).scroll(function() {

	var oTop = $('#counter').offset().top - window.innerHeight;
	if (a == 0 && $(window).scrollTop() > oTop) {
		$('.counter-value').each(function() {
			var $this = $(this),
			countTo = $this.attr('data-count');
			$({
				countNum: $this.text()
			}).animate({
				countNum: countTo
			},

			{

				duration: 2000,
				easing: 'swing',
				step: function() {
					$this.text(Math.floor(this.countNum));
				},
				complete: function() {
					$this.text(this.countNum);
            //alert('finished');
        }

    });
		});
		a = 1;
	}
});

$('.owl-carousel-opinion').owlCarousel({
	center: false,
	items:2,
	loop:true,
	nav: true,
	margin:40,
	responsive:{
		0:{
			items:1,
			nav:true
		},
		600:{
			items:1,
			nav:false
		},
		1000:{
			items:2,
			nav:true,
			loop:false
		}
	}
});
$('.owl-carousel-logos').owlCarousel({
	center: false,
	items:5,
	loop:true,
	nav: true,
	margin:40,
	responsive:{
		0:{
			items:3,
			nav:true
		},
		600:{
			items:3,
			nav:false
		},
		1000:{
			items:5,
			nav:true,
			loop:false
		}
	}

});
var name1 = '.portfolio .single-portfolio-item:nth-child(1)';
var width1 = $( name1 ).width();
$( name1 ).height(width1);
$('#menu-item-367 a').addClass('active');


$(document).ready(function(){
	$(".hamburger").click(function(){
		$(this).toggleClass("is-active");
		$('#list-example').toggleClass("active");
	});
});
