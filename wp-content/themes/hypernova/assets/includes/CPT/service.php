<?php 
function service() {
    if ( have_rows( 'service_options', 'option' ) ) {
        while ( have_rows( 'service_options', 'option' ) ) {
            the_row(); 
            $service_cpt_theme = get_sub_field( 'name' );
            $namelabel = get_sub_field( 'name' );
            $allitem = get_sub_field( 'all_items' );
            $additem = get_sub_field( 'add_item' ); 
            $viewitem = get_sub_field( 'view_item' );
            $edititem = get_sub_field( 'edit_item' );
            $updateitem = get_sub_field( 'update_item' );
        } 
    }

    $labels = array(
        'name'                => _x( $namelabel , 'Post Type General Name', 'kordit' ),
        'singular_name'       => _x( $namelabel , 'Post Type Singular Name', 'kordit' ),
        'menu_name'           => __(  $namelabel , 'kordit' ),
        'all_items'           => __( $allitem, 'kordit' ),
        'view_item'           => __( $viewitem, 'kordit' ),
        'add_new_item'        => __( $additem, 'kordit' ),
        'add_new'             => __( $additem, 'kordit' ),
        'edit_item'           => __( $edititem, 'kordit' ),
        'update_item'         => __( $updateitem, 'kordit' ),
    ); 

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'service' , 'kordit' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'custom-fields', ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'   => 'dashicons-schedule',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );

    // Registering your Custom Post Type
    register_post_type( "service", $args );

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/

add_action( 'init', 'service' );

//dynamic taxonomy

if ( have_rows( 'service_options', 'option' ) ) {
    while ( have_rows( 'service_options', 'option' ) ) {
        the_row(); 
        $turntaxo = get_sub_field( 'turn_on_taxonomy', 'option' );
    }
}
if ($turntaxo) {
    include_once( get_stylesheet_directory() .'/assets/includes/CPT/service/taxonomy.php');
}