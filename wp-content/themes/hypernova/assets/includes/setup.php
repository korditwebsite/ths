<?php
if ( have_rows( 'aktywne_biblioteki', 'option' ) ) : 
	while ( have_rows( 'aktywne_biblioteki', 'option' ) ) : the_row(); 
		
//Eco gallery
		if( get_sub_field('eco_gallery') ) {
			
		}


		//Owl gallery
		if( get_sub_field('owl_gallery') ) {
			wp_enqueue_script( 'owl-gallery', get_template_directory_uri() . '/assets/includes/general/owlgallery/owl.carousel.min.js', array());
			wp_enqueue_script( 'init-owl', get_template_directory_uri() . '/assets/includes/general/owlgallery/init.js', array());
			wp_enqueue_style( 'owl-css', get_template_directory_uri() . '/assets/includes/general/owlgallery/assets/owl.carousel.min.css' );
		}
		
		if( get_sub_field('font_awesome') ) {
			wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/css/font_awesome.css' );
		}
			//Other gallery



		if ( have_rows( 'dodaj_biblioteke' ) ) {
			$licznik = 0;
			while ( have_rows( 'dodaj_biblioteke' ) ) { the_row(); 
				if( get_sub_field('aktywuj_biblioteke') ) {
					$linkbibliotekajs = get_sub_field( 'link_do_biblioteki_js' );
					$linkbibliotekacss = get_sub_field( 'link_do_biblioteki_css' );
					$licznikbibliotek = "init" . $licznik;

					if (get_sub_field("link_do_biblioteki_js")) {
						wp_enqueue_script( $licznikbibliotek, $linkbibliotekajs, array());
					}

					if (get_sub_field("link_do_biblioteki_css")) {
						wp_enqueue_style( $licznikbibliotek, $linkbibliotekacss );
					}
				}
			}
			$licznik = $licznik + 1;
		}


	endwhile;
endif; 

function js_matrix() {
	wp_enqueue_script( "loaded", get_template_directory_uri() . "/assets/includes/general/matrix/loaded.js", array(), '1.0.0', true );
	wp_enqueue_script( "tween", get_template_directory_uri() . "/assets/includes/general/matrix/
		tween.js", array(), '1.0.0', true );
	wp_enqueue_script( "init", get_template_directory_uri() . "/assets/includes/general/matrix/
		init.js", array(), '1.0.0', true );
}
